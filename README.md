# Read Me First

To run this project you have to run a docker container with Postgres:

docker run --name postgres-container -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres

After application started you can find Swagger on http://localhost:8080/management/swagger-ui/index.html#/
