package com.masala.drones.scheduller

import com.masala.drones.service.CheckBatteryService
import org.springframework.scheduling.annotation.Scheduled

open class CheckBatteryLevelScheduler(
    private val checkBatteryService: CheckBatteryService
) {


    @Scheduled(cron = "\${application.drone.battery-checker.cron}")
    open fun schedule() {
        checkBatteryService.checkAllAndLog()
    }

}
