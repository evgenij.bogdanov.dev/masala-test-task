package com.masala.drones.repository

import com.masala.drones.entity.Medication
import org.springframework.data.jpa.repository.JpaRepository

interface MedicationRepository: JpaRepository<Medication, Long> {

    fun getMedicationByCodeIn(codes: List<String>): List<Medication>

}
