package com.masala.drones.repository

import com.masala.drones.entity.DroneMedication
import org.springframework.data.jpa.repository.JpaRepository

interface DroneMedicationRepository: JpaRepository<DroneMedication, Long>