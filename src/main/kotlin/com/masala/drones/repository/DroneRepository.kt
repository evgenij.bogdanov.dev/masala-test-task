package com.masala.drones.repository

import com.masala.drones.entity.Drone
import com.masala.drones.entity.constants.State
import com.masala.drones.entity.projection.DroneBatteryProjection
import java.util.Optional
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query

interface DroneRepository: JpaRepository<Drone, Long> {
    fun findAllByStateAndBatteryCapacityGreaterThanEqual(state: State, batteryCapacity: Double): Collection<Drone>

    @Query(value = "select batteryCapacity from Drone where id = :id")
    fun getBatteryLevel(id: Long): Optional<Double>

    @Query(value = "select id as droneId, batteryCapacity as batteryCapacity from Drone ")
    fun getAllDronesBatteryLevel(): Collection<DroneBatteryProjection>

}
