package com.masala.drones.exception

import com.masala.drones.entity.Drone
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class DroneOverloadException private constructor(message: String) : RuntimeException(message) {

    companion object {
        fun create(drone: Drone, medicationWeight: Int) =
            DroneOverloadException(
                "The total weight of medications ($medicationWeight) exceeds the allowable weight. " +
                        "The drone payload mass is ${drone.weightLimit}"
            )
    }

}
