package com.masala.drones.exception

import com.masala.drones.entity.Drone
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class DroneStateException private constructor(message: String) : RuntimeException(message) {

    companion object {
        fun create(drone: Drone) =
            DroneStateException("Drone with id = ${drone.id} can't be loaded because of it's state. Drone's state is ${drone.state}")
    }

}
