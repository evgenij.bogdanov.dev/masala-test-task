package com.masala.drones.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.BAD_REQUEST)
class DroneNotFoundException private constructor (message: String): RuntimeException(message) {

    companion object {
        fun create(id: Long) = DroneNotFoundException("Drone with id = $id is not found")
    }

}
