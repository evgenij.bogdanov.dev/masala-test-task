package com.masala.drones.exception

import com.masala.drones.entity.Drone
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
class DroneBatteryLowLevelException private constructor(message: String) : RuntimeException(message) {

    companion object {
        fun create(drone: Drone) =
            DroneBatteryLowLevelException(
                "Low battery level. Drone id = ${drone.id}, current battery level = ${drone.batteryCapacity}"
            )
    }

}
