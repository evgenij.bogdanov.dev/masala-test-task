package com.masala.drones.mapper

import com.masala.drones.dto.DronePayload
import com.masala.drones.dto.request.RegisterDroneClientRequestDto
import com.masala.drones.dto.response.DroneClientResponseDto
import com.masala.drones.dto.response.DronePayloadClientResponseDto
import com.masala.drones.entity.Drone
import com.masala.drones.entity.DroneMedication
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.Named

@Mapper(uses = [DroneMedicationMapper::class])
interface DroneMapper {

    @Mapping(source = "batteryCapacity", target = "battery")
    fun toResponse(source: Drone): DroneClientResponseDto

    @Mapping(source = "battery", target = "batteryCapacity")
    fun toEntity(source: RegisterDroneClientRequestDto): Drone

    @Mappings(
        Mapping(target = "droneId", source = "id"),
        Mapping(target = "payload", expression = "java(mapPayload(source.getMedications()))")
    )
    fun toPayload(source: Drone): DronePayloadClientResponseDto

    @Named("mapPayload")
    fun mapPayload(source: Set<DroneMedication>): List<DronePayload>

}
