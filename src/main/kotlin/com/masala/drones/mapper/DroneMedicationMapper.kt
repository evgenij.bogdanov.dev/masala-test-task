package com.masala.drones.mapper

import com.masala.drones.dto.DronePayload
import com.masala.drones.entity.DroneMedication
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings

@Mapper
interface DroneMedicationMapper {

    @Mappings(
        Mapping(target = "medicationCode", source = "source.medication.code"),
        Mapping(target = "count", source = "source.quantity"),
    )
    fun map(source: DroneMedication): DronePayload
}
