package com.masala.drones.mapper

import com.masala.drones.dto.response.MedicationClientResponseDto
import com.masala.drones.entity.Medication
import org.mapstruct.Mapper

@Mapper
interface MedicationMapper {

    fun toResponse(source: Medication): MedicationClientResponseDto

}
