package com.masala.drones.entity.constants

enum class State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
