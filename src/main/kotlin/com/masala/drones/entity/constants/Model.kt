package com.masala.drones.entity.constants

enum class Model {
    LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT
}
