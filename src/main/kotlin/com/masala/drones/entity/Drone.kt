package com.masala.drones.entity

import com.masala.drones.entity.constants.Model
import com.masala.drones.entity.constants.State
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.EnumType
import javax.persistence.Enumerated
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.JoinTable
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity
class Drone {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    var serialNumber: String? = null

    @Enumerated(EnumType.STRING)
    var model: Model? = null

    var weightLimit: Int? = null

    @Column(precision = 3, scale = 2)
    var batteryCapacity: Double? = null

    @Enumerated(EnumType.STRING)
    var state: State? = null

    @OneToMany(mappedBy = "drone")
    var medications: MutableSet<DroneMedication> = mutableSetOf()

}
