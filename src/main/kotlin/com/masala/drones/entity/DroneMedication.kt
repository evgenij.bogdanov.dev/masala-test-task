package com.masala.drones.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.JoinColumn
import javax.persistence.ManyToOne

@Entity
class DroneMedication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null

    @ManyToOne
    var drone: Drone? = null

    @ManyToOne
    @JoinColumn(name = "medication_id")
    var medication: Medication? = null

    var quantity: Int? = null

}
