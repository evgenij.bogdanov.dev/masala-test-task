package com.masala.drones.entity.projection

interface DroneBatteryProjection {

    fun getDroneId(): Long

    fun getBatteryCapacity(): Double

}
