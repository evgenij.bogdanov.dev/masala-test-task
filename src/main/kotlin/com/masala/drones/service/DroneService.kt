package com.masala.drones.service

import com.masala.drones.configuration.DroneProperties
import com.masala.drones.dto.DronePayload
import com.masala.drones.dto.request.LoadDroneClientRequestDto
import com.masala.drones.dto.request.RegisterDroneClientRequestDto
import com.masala.drones.dto.response.BatteryLevelClientResponseDto
import com.masala.drones.dto.response.DroneClientResponseDto
import com.masala.drones.dto.response.DronePayloadClientResponseDto
import com.masala.drones.dto.response.ListWrapper
import com.masala.drones.entity.Drone
import com.masala.drones.entity.DroneMedication
import com.masala.drones.entity.Medication
import com.masala.drones.entity.constants.State
import com.masala.drones.exception.DroneBatteryLowLevelException
import com.masala.drones.exception.DroneNotFoundException
import com.masala.drones.exception.DroneOverloadException
import com.masala.drones.exception.DroneStateException
import com.masala.drones.mapper.DroneMapper
import com.masala.drones.repository.DroneMedicationRepository
import com.masala.drones.repository.DroneRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class DroneService(
    private val droneRepository: DroneRepository,
    private val droneMedicationRepository: DroneMedicationRepository,
    private val mapper: DroneMapper,
    private val medicationService: MedicationService,
    private val droneProperties: DroneProperties,
    private val checkBatteryService: CheckBatteryService,
) {

    @Transactional(readOnly = true)
    fun getAll(): ListWrapper<DroneClientResponseDto> {
        val drones = droneRepository.findAll().map { drone ->
            mapper.toResponse(drone)
        }
        return ListWrapper(results = drones)
    }

    @Transactional
    fun registerDrone(registerDto: RegisterDroneClientRequestDto): DroneClientResponseDto {
        val drone = mapper.toEntity(registerDto)
        return droneRepository.save(drone).let { mapper.toResponse(it) }
    }

    @Transactional
    fun loadDrone(droneId: Long, payload: LoadDroneClientRequestDto) {

        val drone = getAvailableDroneForLoadingOfThrowException(droneId)

        val medications: Map<String, Medication> =
            medicationService.getMedicationsByCodes(payload.payload.map { it.medicationCode })

        checkDroneOverload(drone, medications, payload.payload)

        val droneMedications = payload.payload
            .map { dronePayload ->

                val medication = medications[dronePayload.medicationCode]
                val count = dronePayload.count

                DroneMedication().apply {
                    this.drone = drone
                    this.medication = medication
                    this.quantity = count
                }
            }
        droneMedicationRepository.saveAll(droneMedications)
    }

    @Transactional(readOnly = true)
    fun getDronePayload(droneId: Long): DronePayloadClientResponseDto =
        getDroneByIdOrThrowException(droneId).let { drone ->
            mapper.toPayload(drone)
        }

    @Transactional(readOnly = true)
    fun getAvailableDronesForLoading(): ListWrapper<DroneClientResponseDto> {
        val drones = droneRepository.findAllByStateAndBatteryCapacityGreaterThanEqual(
            State.IDLE,
            droneProperties.minBatteryLevelForLoading
        ).map { drone ->
            mapper.toResponse(drone)
        }
        return ListWrapper(drones)
    }

    @Transactional(readOnly = true)
    fun checkBatteryLevel(id: Long): BatteryLevelClientResponseDto =
        BatteryLevelClientResponseDto(checkBatteryService.getBatteryLevelByDroneId(id))

    private fun getAvailableDroneForLoadingOfThrowException(droneId: Long): Drone {
        val drone = getDroneByIdOrThrowException(droneId)
        if (drone.state != State.IDLE) {
            throw DroneStateException.create(drone)
        }
        drone.batteryCapacity?.let { currentCapacity ->
            if (currentCapacity < droneProperties.minBatteryLevelForLoading) {
                throw DroneBatteryLowLevelException.create(drone)
            }
        }
        return drone
    }

    private fun getDroneByIdOrThrowException(id: Long) =
        droneRepository.findById(id).orElseGet {
            throw DroneNotFoundException.create(id)
        }

    private fun checkDroneOverload(drone: Drone, medications: Map<String, Medication>, payload: List<DronePayload>) {
        val medicationsSumWeight = payload.sumOf { dronePayload ->
            medications[dronePayload.medicationCode]!!.weight!!.times(dronePayload.count)
        }
        drone.weightLimit?.let { weightLimit ->
            if (medicationsSumWeight > weightLimit) {
                throw DroneOverloadException.create(drone, medicationsSumWeight)
            }
        }

    }

}