package com.masala.drones.service

import com.masala.drones.dto.response.ListWrapper
import com.masala.drones.dto.response.MedicationClientResponseDto
import com.masala.drones.entity.Medication
import com.masala.drones.mapper.MedicationMapper
import com.masala.drones.repository.MedicationRepository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class MedicationService(
    private val medicationRepository: MedicationRepository,
    private val mapper: MedicationMapper,
) {

    @Transactional(readOnly = true)
    fun getAll(): ListWrapper<MedicationClientResponseDto> {
        val medications = medicationRepository.findAll()
            .map { mapper.toResponse(it) }
        return ListWrapper(medications)
    }

    fun getMedicationsByCodes(codes: List<String>): Map<String, Medication> =
        medicationRepository.getMedicationByCodeIn(codes)
            .associateBy { medication ->
                medication.code
                    ?: throw IllegalArgumentException("Medication code is absent. Medication id = ${medication.id}")
            }

}
