package com.masala.drones.service

import com.masala.drones.exception.DroneNotFoundException
import com.masala.drones.repository.DroneRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

@Service
class CheckBatteryService(
    private val droneRepository: DroneRepository,
) {

    private val log = LoggerFactory.getLogger(this::class.java)

    @Transactional
    fun getBatteryLevelByDroneId(id: Long): Double =
        droneRepository.getBatteryLevel(id)
            .orElseGet {
                throw DroneNotFoundException.create(id)
            }


    @Transactional
    fun checkAllAndLog() {
        droneRepository.getAllDronesBatteryLevel()
            .forEach { droneBatteryProjection ->
                log.debug(
                    "#batteryCapacity: Drone with id: ${droneBatteryProjection.getDroneId()} " +
                            "current battery level is ${droneBatteryProjection.getBatteryCapacity()}%"
                )
            }
    }

}
