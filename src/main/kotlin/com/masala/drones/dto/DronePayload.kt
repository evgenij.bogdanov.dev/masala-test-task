package com.masala.drones.dto

data class DronePayload(
    val medicationCode: String,
    val count: Int,
)
