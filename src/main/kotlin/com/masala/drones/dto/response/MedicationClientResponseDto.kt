package com.masala.drones.dto.response

data class MedicationClientResponseDto(
    val name: String,
    val weight: Int,
    val code: String,
    val imageCode: String,
)
