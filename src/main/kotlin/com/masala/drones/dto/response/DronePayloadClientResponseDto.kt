package com.masala.drones.dto.response

import com.masala.drones.dto.DronePayload

data class DronePayloadClientResponseDto(
    val droneId: Long,
    val serialNumber: String,
    val payload: List<DronePayload>
)
