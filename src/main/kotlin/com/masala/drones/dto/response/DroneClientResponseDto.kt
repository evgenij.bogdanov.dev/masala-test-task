package com.masala.drones.dto.response

data class DroneClientResponseDto(
    val id: Long,
    val serialNumber: String,
    val model: String,
    val weightLimit: Int,
    val battery: Double,
    val state: String
)
