package com.masala.drones.dto.response

data class BatteryLevelClientResponseDto(
    val batteryLevel: Double,
)
