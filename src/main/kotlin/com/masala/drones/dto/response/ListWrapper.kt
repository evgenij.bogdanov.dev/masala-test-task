package com.masala.drones.dto.response

data class ListWrapper<T>(
    val results: List<T>,
    val total: Long = results.size.toLong()
)
