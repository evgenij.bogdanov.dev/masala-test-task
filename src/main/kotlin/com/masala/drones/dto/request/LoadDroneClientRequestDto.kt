package com.masala.drones.dto.request

import com.masala.drones.dto.DronePayload

data class LoadDroneClientRequestDto(
    val payload: List<DronePayload>,
)
