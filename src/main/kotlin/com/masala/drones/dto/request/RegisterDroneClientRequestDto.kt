package com.masala.drones.dto.request

import com.masala.drones.entity.constants.Model
import com.masala.drones.entity.constants.State
import javax.validation.constraints.Max
import javax.validation.constraints.NotBlank
import javax.validation.constraints.Size

data class RegisterDroneClientRequestDto(

    @get:Size(max = 3)
    @get:NotBlank
    val serialNumber: String,

    val model: Model,

    @get:Max(value = 500)
    val weightLimit: Int,
    val battery: Double,
    val state: State,
)
