package com.masala.drones.configuration

import com.masala.drones.scheduller.CheckBatteryLevelScheduler
import com.masala.drones.service.CheckBatteryService
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@Configuration(proxyBeanMethods = false)
class SchedulingConfiguration {

    @Bean
    fun checkBatteryLevelScheduler(checkBatteryService: CheckBatteryService): CheckBatteryLevelScheduler =
        CheckBatteryLevelScheduler(checkBatteryService)

}
