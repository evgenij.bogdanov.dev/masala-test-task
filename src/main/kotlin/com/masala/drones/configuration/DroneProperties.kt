package com.masala.drones.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConstructorBinding
@ConfigurationProperties(prefix = "application.drone")
data class DroneProperties(
    val minBatteryLevelForLoading: Double,
)
