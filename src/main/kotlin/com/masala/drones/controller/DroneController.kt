package com.masala.drones.controller

import com.masala.drones.dto.request.LoadDroneClientRequestDto
import com.masala.drones.dto.request.RegisterDroneClientRequestDto
import com.masala.drones.dto.response.BatteryLevelClientResponseDto
import com.masala.drones.dto.response.DroneClientResponseDto
import com.masala.drones.dto.response.DronePayloadClientResponseDto
import com.masala.drones.dto.response.ListWrapper
import com.masala.drones.service.DroneService
import io.swagger.v3.oas.annotations.Operation
import javax.validation.Valid
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/drone")
@Validated
class DroneController(
    private val droneService: DroneService,
) {

    @GetMapping("/all")
    @Operation(summary = "Get all drones")
    fun getAll(): ListWrapper<DroneClientResponseDto> = droneService.getAll()

    @PostMapping
    @Operation(summary = "Register new drone")
    fun registerDrone(@Valid @RequestBody registerDto: RegisterDroneClientRequestDto): DroneClientResponseDto =
        droneService.registerDrone(registerDto)

    @PostMapping("/load/{droneId}")
    @Operation(summary = "Load drone by id")
    fun loadDrone(@RequestBody requestDto: LoadDroneClientRequestDto, @PathVariable droneId: Long) =
        droneService.loadDrone(droneId = droneId, payload = requestDto)

    @GetMapping("/load/available")
    @Operation(summary = "Get available drones for loading")
    fun getAvailableForLoading(): ListWrapper<DroneClientResponseDto> =
        droneService.getAvailableDronesForLoading()

    @GetMapping("/payload/{droneId}")
    @Operation(summary = "Get drone payload by id")
    fun getDronePayload(@PathVariable droneId: Long): DronePayloadClientResponseDto =
        droneService.getDronePayload(droneId)

    @GetMapping("/battery/{droneId}")
    @Operation(summary = "Check drone's battery level")
    fun checkBatteryLevel(@PathVariable droneId: Long): BatteryLevelClientResponseDto =
        droneService.checkBatteryLevel(droneId)

}
