package com.masala.drones.controller

import com.masala.drones.dto.response.ListWrapper
import com.masala.drones.dto.response.MedicationClientResponseDto
import com.masala.drones.service.MedicationService
import io.swagger.v3.oas.annotations.Operation
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/medication")
class MedicationController(
    private val medicationService: MedicationService
) {

    @GetMapping
    @Operation(summary = "Get all medications")
    fun getAll(): ListWrapper<MedicationClientResponseDto> = medicationService.getAll()


}