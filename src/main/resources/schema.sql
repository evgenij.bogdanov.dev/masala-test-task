TRUNCATE TABLE drone_medication cascade;
TRUNCATE TABLE medication cascade;
TRUNCATE TABLE drone cascade;

INSERT INTO medication (name, weight, code, image_code)
VALUES ('Aspirin', 30, 'A_1', 'img_1'),
       ('Alanine', 50, 'A_2', 'img_2'),
       ('Injector', 25, 'A_3', 'img_3');

INSERT INTO drone (serial_number, model, weight_limit, battery_capacity, state)
VALUES ('10', 'LIGHTWEIGHT', 100, 100.0, 'IDLE'),
       ('11', 'LIGHTWEIGHT', 200, 90.0, 'LOADING'),
       ('12', 'MIDDLEWEIGHT', 300, 90.0, 'LOADED'),
       ('13', 'CRUISERWEIGHT', 350, 60.0, 'DELIVERING'),
       ('14', 'HEAVYWEIGHT', 400, 50.0, 'DELIVERED'),
       ('15', 'HEAVYWEIGHT', 500, 30.0, 'RETURNING'),
       ('16', 'HEAVYWEIGHT', 500, 20.7, 'IDLE'),
       ('16', 'LIGHTWEIGHT', 500, 10, 'IDLE');

