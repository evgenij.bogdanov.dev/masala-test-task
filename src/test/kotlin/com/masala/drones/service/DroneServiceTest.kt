package com.masala.drones.service

import com.masala.drones.configuration.DroneProperties
import com.masala.drones.dto.DronePayload
import com.masala.drones.dto.request.LoadDroneClientRequestDto
import com.masala.drones.dto.request.RegisterDroneClientRequestDto
import com.masala.drones.dto.response.DroneClientResponseDto
import com.masala.drones.entity.Drone
import com.masala.drones.entity.Medication
import com.masala.drones.entity.constants.Model
import com.masala.drones.entity.constants.State
import com.masala.drones.exception.DroneOverloadException
import com.masala.drones.exception.DroneStateException
import com.masala.drones.mapper.DroneMapper
import com.masala.drones.repository.DroneMedicationRepository
import com.masala.drones.repository.DroneRepository
import java.util.Optional
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.never
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

/**
 * Tests for [DroneService]
 */
internal class DroneServiceTest {

    private lateinit var droneRepository: DroneRepository
    private lateinit var droneMedicationRepository: DroneMedicationRepository
    private lateinit var mapper: DroneMapper
    private lateinit var medicationService: MedicationService
    private lateinit var droneProperties: DroneProperties
    private lateinit var checkBatteryService: CheckBatteryService
    private lateinit var service: DroneService

    @BeforeEach
    fun setUp() {
        droneRepository = mock()
        droneMedicationRepository = mock()
        mapper = mock()
        medicationService = mock()
        droneProperties = DroneProperties(100.0)
        checkBatteryService = mock()
        service = DroneService(
            droneRepository,
            droneMedicationRepository,
            mapper,
            medicationService,
            droneProperties,
            checkBatteryService
        )
    }

    companion object {
        private val REGISTER_DRONE_DTO = RegisterDroneClientRequestDto(
            serialNumber = "123",
            model = Model.CRUISERWEIGHT,
            weightLimit = 100,
            battery = 100.0,
            state = State.IDLE
        )
    }


    @Test
    fun `should register new drone`() {
        val droneStub = Drone()
        val responseStub = DroneClientResponseDto(1, "", "", 1, 0.0, "")
        whenever(mapper.toEntity(REGISTER_DRONE_DTO)).thenReturn(droneStub)
        whenever(droneRepository.save(droneStub)).thenReturn(droneStub)
        whenever(mapper.toResponse(droneStub)).thenReturn(responseStub)
        val actual = service.registerDrone(REGISTER_DRONE_DTO)

        Assertions.assertThat(actual).isEqualTo(responseStub)
        verify(mapper, times(1)).toEntity(REGISTER_DRONE_DTO)
        verify(droneRepository, times(1)).save(droneStub)
    }

    @Test
    fun `should not load drone when payload weight is more the drone max`() {
        val droneStub = Drone().apply {
            id = 1
            weightLimit = 1
            batteryCapacity = 100.0
            state = State.IDLE
        }
        val payLoad = LoadDroneClientRequestDto(payload = listOf(DronePayload(medicationCode = "1", 1)))
        whenever(droneRepository.findById(any())).thenReturn(Optional.of(droneStub))
        whenever(medicationService.getMedicationsByCodes(any())).thenReturn(
            mapOf(
                Pair(
                    "1",
                    Medication().apply { weight = 100 })
            )
        )

        Assertions.assertThatThrownBy { service.loadDrone(1, payLoad) }
            .isInstanceOf(DroneOverloadException::class.java)
            .hasMessage("The total weight of medications (100) exceeds the allowable weight. The drone payload mass is 1")

        Mockito.verify(droneRepository, never()).save(any())

    }

    @Test
    fun `should not load drone when it's in wrong state`() {
        val droneStub = Drone().apply {
            id = 1
            weightLimit = 1
            batteryCapacity = 100.0
            state = State.LOADED
        }
        whenever(droneRepository.findById(any())).thenReturn(Optional.of(droneStub))
        val payLoad = LoadDroneClientRequestDto(payload = listOf(DronePayload(medicationCode = "1", 1)))

        Assertions.assertThatThrownBy { service.loadDrone(1, payLoad) }
            .isInstanceOf(DroneStateException::class.java)
            .hasMessage("Drone with id = ${droneStub.id} can't be loaded because of it's state. Drone's state is ${droneStub.state}")

        Mockito.verify(droneRepository, never()).save(any())

    }


}