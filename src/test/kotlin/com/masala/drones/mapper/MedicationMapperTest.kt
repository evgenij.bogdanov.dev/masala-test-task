package com.masala.drones.mapper

import com.masala.drones.entity.Medication
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.mapstruct.factory.Mappers

/**
 * Test for [MedicationMapper]
 */
internal class MedicationMapperTest {

    private lateinit var mapper: MedicationMapper

    @BeforeEach
    fun setUp() {
        mapper = Mappers.getMapper(MedicationMapper::class.java)
    }

    companion object {
        private val SOURCE = Medication().apply {
            id = 1
            name = "test name"
            weight = 1
            code = "test code"
            imageCode = "123"
        }
    }


    @Test
    fun `should map correct`() {
        val actual = mapper.toResponse(SOURCE)
        Assertions.assertThat(actual.code).isEqualTo("test code")
        Assertions.assertThat(actual.imageCode).isEqualTo("123")
        Assertions.assertThat(actual.name).isEqualTo("test name")
        Assertions.assertThat(actual.weight).isEqualTo(1)
    }

}
